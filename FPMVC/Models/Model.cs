﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace FPMVC
{
    public class Users
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)] //генерация значения ключа
        public int IDUser { get; set; }
        [Required] // поле обязательно для заполнения
        [MaxLength(30)]
        public string Login { get; set; }
        [Required]
        [MaxLength(30), MinLength(4)]
        public string Password { get; set; }
        [Required]
        [MaxLength(50)]
        public string Surname { get; set; }
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }
        [Required]
        [MaxLength(10)]
        public string Phone { get; set; }
    }
    public class Goods
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [MaxLength(30)]
        public string ArticleNumber { get; set; }
        [Required]
        [MaxLength(100)]
        public string ProductName { get; set; }
        [Required]

        public float Price { get; set; }
        [Required] //обязателен к вполнению
                                            //Для отслеживания изменений
        //[Timestamp]
        //[ConcurrencyCheck]                  //   !!!    Необходимо блокировать поток при внесении изменений                
        public int StockBalance { get; set; }

        public float TotalSum { get; set; }



    }
    public class TypesofOperation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDOperation { get; /*private */set; }

        public string TypeOfOperation { get; /*private*/ set; }
    }
    public class AccountingGoodsIncomingOutgoing
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        public Users IDUser { get; set; }
        [Required]
        public Goods ArticleNumber { get; set; }
        
        public DateTime Date { get; set; }
        [Required]
        public TypesofOperation IDOperation { get; set; }
        [Required]
        public int Count { get; set; }
    }
}

