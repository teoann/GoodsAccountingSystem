﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPMVC.Models;

namespace FPMVC
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Goods> Goods { get; set; }
        public DbSet<TypesofOperation> TypesofOperation { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<AccountingGoodsIncomingOutgoing> AccountingGoodsIncomingOutgoing { get; set; }
        public DatabaseContext() : base(/*"AccountingSystemContext"*/)
        {
            
        }
        

        //метод для добавления настроек конфигурации
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UsersConfiguration());
        }

    }
    //настройки для таблицы Пользователей
    public class UsersConfiguration: EntityTypeConfiguration<Users>
    {
        public UsersConfiguration()
        {
            this.Property(u => u.Phone).IsRequired().IsFixedLength().HasMaxLength(10);
        }
    }
    
}
