﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPMVC.Models;

namespace FPMVC.Models
{    public class AddTypeOfOperation
    {
        public static void AddTypesOfOperation()
        {
            DatabaseContext databaseContext = new DatabaseContext();
            databaseContext.TypesofOperation.Add(new TypesofOperation { TypeOfOperation = "списано" });
            databaseContext.TypesofOperation.Add(new TypesofOperation { TypeOfOperation = "поступило" });
            databaseContext.SaveChanges();
        }
    }
}