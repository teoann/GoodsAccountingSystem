﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodeFirst;

namespace FPMVC.Controllers
{
    public class HomeController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        public ActionResult Index()
        {
            //получ все из БД
            IEnumerable<TypesofOperation> ot = db.TypesofOperation;
            //передаем все получ дан в динамич св-во контроллера ViewBag
            //ViewBag.Users = ot;
            ViewBag.TypesofOperation = ot;
            ViewBag.Controller = "Home";
            ViewBag.Action = "Index";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}