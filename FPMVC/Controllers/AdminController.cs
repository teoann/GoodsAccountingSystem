﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodeFirst;

namespace FPMVC.Controllers
{
    public class AdminController : Controller
    {
        // GET: AdminHome
        public ActionResult Index()
        {
            ViewBag.Title = "Пользователи";
            
            CodeFirst.DatabaseContext db = new CodeFirst.DatabaseContext();
            IEnumerable<Users> us = db.Users.ToList();

            return View(us);
        }
    }
}