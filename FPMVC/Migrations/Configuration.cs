namespace FPMVC.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using FPMVC.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<FPMVC.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "FPMVC.DatabaseContext";
        }

        protected override void Seed(FPMVC.DatabaseContext context)
        {
            DatabaseContext dbcontext = new DatabaseContext();
            dbcontext.Users.Add(new Users { Login = "admin", Password = "admin", Surname = "Administrator", Email = "admin@mail.ru", Phone = "0987456123" });
            AddTypeOfOperation.AddTypesOfOperation();
            dbcontext.SaveChanges();
        }
    }
}
