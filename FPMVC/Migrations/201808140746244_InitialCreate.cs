namespace FPMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountingGoodsIncomingOutgoings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Count = c.Int(nullable: false),
                        ArticleNumber_ArticleNumber = c.String(nullable: false, maxLength: 30),
                        IDOperation_IDOperation = c.Int(nullable: false),
                        IDUser_IDUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Goods", t => t.ArticleNumber_ArticleNumber, cascadeDelete: true)
                .ForeignKey("dbo.TypesofOperations", t => t.IDOperation_IDOperation, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.IDUser_IDUser, cascadeDelete: true)
                .Index(t => t.ArticleNumber_ArticleNumber)
                .Index(t => t.IDOperation_IDOperation)
                .Index(t => t.IDUser_IDUser);
            
            CreateTable(
                "dbo.Goods",
                c => new
                    {
                        ArticleNumber = c.String(nullable: false, maxLength: 30),
                        ProductName = c.String(nullable: false, maxLength: 100),
                        Price = c.Single(nullable: false),
                        StockBalance = c.Int(nullable: false),
                        TotalSum = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ArticleNumber);
            
            CreateTable(
                "dbo.TypesofOperations",
                c => new
                    {
                        IDOperation = c.Int(nullable: false, identity: true),
                        TypeOfOperation = c.String(),
                    })
                .PrimaryKey(t => t.IDOperation);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        IDUser = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 30),
                        Password = c.String(nullable: false, maxLength: 30),
                        Surname = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 100),
                        Phone = c.String(nullable: false, maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.IDUser);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountingGoodsIncomingOutgoings", "IDUser_IDUser", "dbo.Users");
            DropForeignKey("dbo.AccountingGoodsIncomingOutgoings", "IDOperation_IDOperation", "dbo.TypesofOperations");
            DropForeignKey("dbo.AccountingGoodsIncomingOutgoings", "ArticleNumber_ArticleNumber", "dbo.Goods");
            DropIndex("dbo.AccountingGoodsIncomingOutgoings", new[] { "IDUser_IDUser" });
            DropIndex("dbo.AccountingGoodsIncomingOutgoings", new[] { "IDOperation_IDOperation" });
            DropIndex("dbo.AccountingGoodsIncomingOutgoings", new[] { "ArticleNumber_ArticleNumber" });
            DropTable("dbo.Users");
            DropTable("dbo.TypesofOperations");
            DropTable("dbo.Goods");
            DropTable("dbo.AccountingGoodsIncomingOutgoings");
        }
    }
}
